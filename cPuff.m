function c = cPuff(p)
%CPUFF Concentration from puff source.
%   p is struct with input parameters. If emissionPeriod input
%   parameter is scalar, then concentration is calculated 
%   assuming instant puff source. If emissionPeriod is array
%   with range, e.g., [2,5] or [2,inf], then concentration 
%   is calculated assuming continuous source emitting during
%   given range. If source is instantanious, Q parameter is
%   interpreted as amount, and if source is continuous, 
%   Q is interpreted as emisssion flux (amount/time).
%   Coordinates:
%       p.x (m)
%       p.y (m)
%       p.z (m)
%       p.t (s)
%   Source:
%       p.xSource, location of source in x dimension (m)
%       p.ySource, location of source in y dimension (m)
%       p.zSource, location of source in z dimension (m)
%       p.emissionPeriod, time(range) when puff occurs (s)
%       p.Q, amount of emission (particles or mass, arbitrary
%            unit) or emission flux (particle or 
%            mass per time, a.u./s)
%       p.K, turbulent diffusion coefficient (m^2/s)
%   Room:
%       p.xRoom, length of the room in x dimension (m)
%       p.yRoom, width  of the room in y dimension (m)
%       p.zRoom, height of the room in z dimension (m)
%       p.a, ventilation rate (1/s)
%       p.wd, deposition rate (m/s)
%   About input dimensions:
%       Input dimensions for for x, y, z and t should be:
%       * They all are equal and output is elementwise 
%         calculation
%       OR
%       * x, y and z are equal, but t is scalar
%       OR
%       * x, y and z are scalar and t is arbitrary. 
%         Output will be same dimensions as t
%   System requirement: MATLAB R2017b or newer
%                       Function cPuffInstant.m
%   Licence:  Artworks CC BY-SA
%             Code     Boost Software License - 
%                      Version 1.0 - August 17th, 2003
%   Author:   Niko Leskinen

%checkInput(p)
if isscalar(p.emissionPeriod)
    % Instant source
    p.tEmission = p.emissionPeriod;
    c = cPuffInstant(p);
else
    % Continuous source
    if isscalar(p.t)
        c = cPuffContinuous(p);
    elseif isscalar(p.x) && isscalar(p.y) && isscalar(p.z)
        c = nan(size(p.t));
        t = p.t;
        for i = 1:numel(t)
            p.t = t(i);
            c(i) = cPuffContinuous(p);
        end
    else
        error('Error: Reduce dimensions')        
    end
end
end


function checkInput(p)
assert(isfield(p,'emissionPeriod'), ...
    'Input p.emissionPeriod missing.')
assert(0 < numel(p.emissionPeriod) && ...
    numel(p.emissionPeriod) < 3,...
    'emissionPeriod should be scalar', ...
    'or array with two elements')
end

function c = cPuffInstantWrapper(p,t)
p.t = t;
c = cPuffInstant(p);
end

function c = cPuffContinuous(p)
assert(isscalar(p.t),...
    'p.t must be scalar to avoid ambiguousness')
t = p.t;
p.tEmission = 0;
emissionStarted = p.emissionPeriod(1);
emissionEnded   = p.emissionPeriod(2);
integrand = @(t) cPuffInstantWrapper(p,t);
if t < emissionStarted+eps
    % emission has not started yet
    c = zeros(size(p.x));
elseif emissionStarted < t && t < emissionEnded
    % during emission
    timeFromLastPuff = 0;
    timeFromEarliestPuff = t - emissionStarted;
    c = integral(integrand,...
        timeFromLastPuff,timeFromEarliestPuff,...
        'ArrayValued',true,'RelTol',1e-1);
else
    % after emission period
    timeFromLastPuff = t - emissionEnded;
    timeFromEarliestPuff = t - emissionStarted;
    c = integral(integrand,...
        timeFromLastPuff,timeFromEarliestPuff,...
        'ArrayValued',true,'RelTol',1e-1);
end

end