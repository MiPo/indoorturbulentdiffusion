function c = cPuffInstant(p)
%CPUFF Concentration from puff source.
%   p is struct with input parameters.
%   Coordinates:
%       p.x (m)
%       p.y (m)
%       p.z (m)
%       p.t (s)
%   Source:
%       p.xSource, location of source in x dimension (m)
%       p.ySource, location of source in y dimension (m)
%       p.zSource, location of source in z dimension (m)
%       p.tEmission, time when puff occurs (s)
%       p.Q, amount of emission (particles or mass, 
%            arbitrary unit) or emission flux 
%           (particle or mass per time, a.u./s)
%       p.K, turbulent diffusion coefficient (m^2/s)
%   Room:
%       p.xRoom, length of the room in x dimension (m)
%       p.yRoom, width  of the room in y dimension (m)
%       p.zRoom, height of the room in z dimension (m)
%       p.a, ventilation rate (1/s)
%       p.wd, deposition rate (m/s)
%   About input dimensions:
%       Input dimensions for for x, y, z and t should be:
%       * They all are equal and output is elementwise 
%         calculation
%       OR
%       * x, y and z are equal, but t is scalar
%       OR
%       * x, y and z are scalar and t is arbitrary. 
%         Output will be same dimensions as t
%   System requirement: MATLAB R2017b or newer
%   Licence:  Artworks CC BY-SA
%             Code     Boost Software License - 
%                      Version 1.0 - August 17th, 2003
%   Author:   Niko Leskinen

%checkInput(p);
% shift time such that emission starts at 0
p.t = p.t-p.tEmission;
c = firstTerm(p).*secondTerm(p).*...
    rTerm(p,'x').*rTerm(p,'y').*rTerm(p,'z');
% let c be zero if emission has not occurred
if isscalar(p.t) && p.t < 0+eps
    c = zeros(size(c));
else
    T = p.t;
    c(T<0+eps) = 0;
end
if any(isnan(c(:)))
    error('Error')
end
end

function checkInput(p)
inputFields = { 'x','y','z','t',...
    'xSource','ySource','zSource','tEmission','Q','K',...
    'xRoom','yRoom','zRoom','a','wd'};
for i = 1:numel(inputFields)
    inField = inputFields{i};
    assert(isfield(p,inField),sprintf(...
        'Input p.%s missing',inField));
end
assert(checkDimensions(p),'Check input dimensions')
end


function b = checkDimensions(p)
if isequal(size(p.x),size(p.y),size(p.z))
    if isequal(size(p.x),size(p.t))
        % All are equal, this is okay
        b = true;return;
    elseif isscalar(p.t)
        % time is scalar, this is okay too
        b = true;return;
    elseif isscalar(p.x) && isscalar(p.y) && isscalar(p.z)
        % spatial coordinates are scalar, this is okay too,
        % output will be same size as input time array
        b = true;return;
    else
        b = false;return
    end
else
    b = false;return
end
end


function value = firstTerm(p)
Q = p.Q;
K = p.K;
t = p.t;
value = Q./((4*pi*K*t).^1.5);
end


function value = secondTerm(p)
a = p.a;
wd = p.wd;
if a==0 && wd==0;value=1;return;end
xRoom = p.xRoom;
yRoom = p.yRoom;
zRoom = p.zRoom;
t = p.t;
% deposition surface
A = 2*(xRoom*yRoom + xRoom*zRoom + yRoom*zRoom);
% volume
V = xRoom*yRoom*zRoom;
% decayterm
value = exp(-a*t - wd*A*t/V);
end


function value = rTerm(p,dim)
switch dim
    case 'x'
        x = p.x;
        xRoom = p.xRoom;
        xSource = p.xSource;
    case 'y'
        x = p.y;
        xRoom = p.yRoom;
        xSource = p.ySource;
    case 'z'
        x = p.z;
        xRoom = p.zRoom;
        xSource = p.zSource;
    otherwise
        error('Incorrect dimension')
end
t = p.t;
K = p.K;
region = sqrt(2*K*t)/xRoom;
if isscalar(t)
    if true%region <= 0.64
        value = firstRegionApproximation(x,xRoom,xSource,K,t);
        return
    elseif 0.63 < region && region <= 1.08
        value = secondRegionApproximation(x,xRoom,xSource,K,t);
        return
    else
        value = ones(size(p.x)).* ...
            thirdRegionApproximation(xRoom,K,t);
        return
    end
else    
    R1 = firstRegionApproximation(x,xRoom,xSource,K,t);
    R2 = secondRegionApproximation(x,xRoom,xSource,K,t);
    R3 = thirdRegionApproximation(xRoom,K,t);
    R = nan(size(t));
    R(region <= 0.64) = R1(region <= 0.64);
    R(0.63 < region & region <= 1.08) = ...
        R2(0.63 < region & region <= 1.08);
    R(1.08 < region) = R3(1.08 < region);
    value = R;
end
end

function value = firstRegionApproximation(x,xRoom,xSource,K,t)
R = 0;
for i = -10:10
    numeratorMinus = (x+2*i*xRoom-xSource).^2;
    numeratorPlus  = (x+2*i*xRoom+xSource).^2;
    denominator = 4*K*t;
    R = R + ...
        exp(- numeratorMinus./denominator) + ...
        exp(- numeratorPlus./denominator);
end
value = R;
end


function value = secondRegionApproximation(x,xRoom,xSource,K,t)
beta = exp(-pi^2*K*t/(xRoom^2));
termA = (2*sqrt(pi*K*t)/xRoom);
termB = (1-beta.^2);
termC = (1+beta.^2 + 2*beta.*...
    cos(pi*x/xRoom).*cos(pi*xSource/xRoom));
value = termA.*termB.*termC;
end

function value = thirdRegionApproximation(xRoom,K,t)
value = 2*sqrt(pi*K*t)/xRoom;
end