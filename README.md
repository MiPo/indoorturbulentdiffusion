# IndoorTurbulentDiffusion

This numerical model calculates diffusion/dispersion of indoor air pollutants in room environments, e.g., nanoparticles from 3D printing. 

The model is published in a scientific peer-reviewed journal, see Poikkimäki et al. (2019), ES&T, https://doi.org/10.1021/acs.est.9b05317.
Please reference the article, when using the model code.

**Code**\
”indoorTurbulentDiffusion.m” main script\
”cPuff.m” calculates continuous pollutant source\
”cPuffInstant.m” calculates instantaneous pollutant source\
See more detailed documentation in the source code.

**System requirement:** MATLAB R2017b or newer\
**Code licence:**        Boost Software License - Version 1.0 - August 17th, 2003\
**Artworks licence:**    Creative Commons CC BY-SA or Copyright © 2019 American Chemical Society (Fig. 5 of the article)\
**Authors:**            Niko Leskinen and Mikko Poikkimäki\
**Contact:**            Mikko Poikkimäki, Finnish Institute of Occupational Health\
**Email:** mikko.poikkimaki(at)ttl.fi
