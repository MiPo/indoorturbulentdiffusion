function indoorTurbulentDiffusion()
%INDOORTURBULENTDIFFUSION Aerosol concentration from pollutant source.
%   Dispersion of aerosol particles in a room 
%   from a 3D printing source based on the measurements 
%   by Poikkimaki et al. ES&T (2019).
%   System requirement: MATLAB R2017b or newer
%                       Functions cPuff.m and cPuffInstant.m
%   Licence:  Artworks Creative Commons CC BY-SA
%             Code     Boost Software License - 
%                      Version 1.0 - August 17th, 2003
%   Authors:  Niko Leskinen and Mikko Poikkimaki

% input
p = struct;
% total and emission times
p.t = linspace(0,3500,100); % s
emissionStart = 500; % s
emissionEnd = 2500; % s
p.emissionPeriod = [emissionStart, emissionEnd];
% room dimensions - laboratory room
p.xRoom = 9; % m
p.yRoom = 6;
p.zRoom = 3; 
% source position
p.xSource = 0.7; % m
p.ySource = 3.2;
p.zSource = 0.9;
% measurement/observer position
distances = [0.35, 1, 2]; % m
x = 0.7 + distances;
p.y = 3.2;
p.z = 0.9;
% turbulent diffusion coefficient
p.K = 0.002; % m^2/s
% ventilation rate - laboratory room
p.a = 18/3600; % 1/s
% deposition rate
p.wd = 0; % m/s
% emission rate NCA size 1-3 nm
Q = [4.25745e9, 5.39233e7, 2.87422e6]; % 1/s
%Q = [1e10 1e8 1e6]; % 1/s
% total emission rate
Qtot = Q./[.44605, .24331, .37524]; % 1/s
%Qtot = Q./[.419 .342 .155]; % 1/s
% emission rate larger particles than NCA
Qlarge = Qtot - Q;

% other environment (p2) representing a normal home room
p2 = p;
p2.xRoom = 4; % m
p2.yRoom = 4;
p2.zRoom = 2.5; 
p2.a = 0.5/3600; % 1/s

% output
% model concentration for different emission rates and 
% distances
[cNCA, cNCAMax, ~, ctotMax, ~, clargeMax] = ncaConc(p, x, distances, Q, Qtot, Qlarge);
[~, ~, ~, ctotMax2, ~, clargeMax2] = ncaConc(p2, x, distances, Q, Qtot, Qlarge);


% rough concentration calculation assuming uniform mixing
volume = p.xRoom*p.yRoom*p.zRoom;
cUni = Q*(emissionEnd-emissionStart)/volume*1e-6; % 1/cm3
disp(['Concentrations assuming uniform mixing are ', ...
    num2str(cUni),' 1/cm^3.'])

% plot concentration over time in the laboratory room
set(0,'defaulttextinterpreter','latex')
set(groot,{'DefaultAxesXColor','DefaultAxesYColor',...
    'DefaultAxesZColor'},{'k','k','k'})

figure
legendInfo = cell(1,length(Q)*length(distances)); ileg = 1;
for iq = 1:length(Q)
    for idist = 1:length(distances)
        semilogy(p.t,cNCA{iq,idist}*1e-6,'-')
        hold on
        legendInfo{ileg} = ['Emission 10^{', ...
            num2str(log10(Q(iq))), ...
            '} 1/s, Distance ', ...
            num2str(distances(idist)),' m' ]; 
        ileg = ileg +1; 
    end
end
xlabel('Time (s)')
ylabel('Concentration (particles/cm$^3$)')
legend(legendInfo, 'Location', 'Best')

% barplot of steady-state concentrations
figure;
set(gcf,'Units','Inch','Position',[0 0 6.8 3.25])
set(gcf,'Units','Inch','PaperSize',[6.8 3.25]);

% Laboratory room
setappdata(gcf, 'SubplotDefaultAxesLocation', [0.1, 0.14, 0.95, 0.8]);
sp1 = subplot(121);
h1 = bar(ctotMax'*1e-6, 'FaceAlpha',0.4, 'Linewidth', 0.5);
h1(1).FaceColor = 'k'; 
h1(2).FaceColor = 'r'; 
h1(3).FaceColor = 'b';
set(gca,'nextplot','add')
h2 = bar(clargeMax'*1e-6, 'FaceAlpha',0.6, 'Linewidth', 0.5);
h2(1).FaceColor = 'k'; 
h2(2).FaceColor = 'r'; 
h2(3).FaceColor = 'b';
% set ticks and labels
set(gca,'YScale','log')
axis([0.5 3.5 1e2 1e7])
set(gca,'xticklabel',{'0.35 m','1 m','2 m'})
ax = gca; ax.XAxis.TickLength = [0,0];
% OEL horizontal lines
hlower = refline([0,20000]);% OEL lower
hupper = refline([0,40000]);% OEL upper
highway = refline([0,100000]);% highway
urbanBg = refline([0,5000]);% bg
hlower.Color = 'k';
hupper.Color = 'k';
highway.Color = 'k';
urbanBg.Color = 'k';
hlower.LineStyle = '--';
hupper.LineStyle = '--';
highway.LineStyle = ':';
urbanBg.LineStyle = '-.';
% text boxes
text(3.3, 5.5e6,'a)');
text(2.9, 150000,'Highway');
text(2.9, 27000,'OELs');
text(2.9, 7000,'Urb. BG');
annotation('textarrow',[0.18 0.14],[0.85 0.74],...
    'String','NCA ','Interpreter','latex')
text(0.7, 2.5e6, [num2str(round(cNCAMax(1,1)/ctotMax(1,1)*100),2), ' \%'])
annotation('textarrow',[0.20 0.17],[0.84 0.48],...
    'String','','Interpreter','latex')
text(0.9, 6e4, [num2str(round(cNCAMax(2,1)/ctotMax(2,1)*100),2), ' \%'])
annotation('textarrow',[0.22 0.20],[0.84 0.24],...
    'String','','Interpreter','latex')
text(1.27, 1e3, [num2str(round(cNCAMax(3,1)/ctotMax(3,1)*100),2), ' \%'])
set(gca,'TickLabelInterpreter','latex')
set(gca,'fontsize', 10)

% Home room
setappdata(gcf, 'SubplotDefaultAxesLocation', [0.00, 0.14, 0.95, 0.8]);
sp2 = subplot(122);
h1 = bar(ctotMax2'*1e-6, 'FaceAlpha',0.4, 'Linewidth', 0.5);
h1(1).FaceColor = 'k'; 
h1(2).FaceColor = 'r'; 
h1(3).FaceColor = 'b';
set(gca,'nextplot','add')
h2 = bar(clargeMax2'*1e-6, 'FaceAlpha',0.6, 'Linewidth', 0.5);
h2(1).FaceColor = 'k'; 
h2(2).FaceColor = 'r'; 
h2(3).FaceColor = 'b';
% set ticks and labels
set(gca,'YScale','log')
axis([0.5 3.5 1e2 1e7])
set(gca,'xticklabel',{'0.35 m','1 m','2 m'})
set(gca,'yTickLabel',[]);
ax = gca; ax.XAxis.TickLength = [0,0];
% OEL horizontal lines
hlower = refline([0,20000]);% OEL lower
hupper = refline([0,40000]);% OEL upper
highway = refline([0,100000]);% highway
urbanBg = refline([0,5000]);% bg
hlower.Color = 'k';
hupper.Color = 'k';
highway.Color = 'k';
urbanBg.Color = 'k';
hlower.LineStyle = '--';
hupper.LineStyle = '--';
highway.LineStyle = ':';
urbanBg.LineStyle = '-.';
% text boxes
text(3.3, 5.5e6,'b)');
text(1.69, 1.8e6,'nGEN');
text(1.69, 1.1e6,'250$^\circ$C');
text(1.91, 5e4,'ABS');
text(1.91, 2.8e4,'250$^\circ$C');
text(2.14, 1.2e3,'PLA');
text(2.14, 7.5e2,'210$^\circ$C');
set(gca,'TickLabelInterpreter','latex')
set(gca,'fontsize', 10)

% Axis labels
pos1=get(sp1,'position');
pos2=get(sp2,'position');
height=pos1(2)+pos1(4)-pos2(2);
width=pos2(1)+pos2(3)-pos1(1);
h5=axes('position',[pos1(1) pos2(2) width height],'visible','off'); 
h5.XLabel.Visible='on';
h5.YLabel.Visible='on';
axes(h5)
ylabel('test')
xlabel('Distance from the printer')
ylabel('Concentration (cm$^{-3}$)')

end

function [cNCA, cNCAMax, ctot, ctotMax, clarge, clargeMax] = ncaConc(p, x, distances, Q, Qtot, Qlarge)
cNCA = cell(length(Q),length(distances)); 
ctot = cNCA; clarge = cNCA;
cNCAMax = zeros(length(Q),length(distances)); 
ctotMax = cNCAMax; clargeMax = cNCAMax;
for iq = 1:length(Q)
    for idist = 1:length(distances)
        p.x = x(idist);
        % NCA
        p.Q = Q(iq);
        conc = cPuff(p);
        cNCA{iq, idist} = conc;
        cNCAMax(iq, idist) = max(conc);
        % total
        p.Q = Qtot(iq);
        conc = cPuff(p);
        ctot{iq, idist} = conc;
        ctotMax(iq, idist) = max(conc);
        % large
        p.Q = Qlarge(iq);
        conc = cPuff(p);
        clarge{iq, idist} = conc;
        clargeMax(iq, idist) = max(conc);
    end
end

end